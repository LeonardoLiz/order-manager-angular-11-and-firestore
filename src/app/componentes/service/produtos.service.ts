import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage'
import { AuthService } from 'src/app/auth/service/auth.service';
import { product } from '../produtos/produto.model';


@Injectable({
  providedIn: 'root'
})
export class ProdutosService {


  constructor(
    private authService: AuthService,
    private aFire: AngularFirestore,
    private af: AngularFireStorage,
    private snackBar: MatSnackBar
    ) { }

  showMessage(msg: string): void {
    this.snackBar.open(msg, 'OK', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }

  createProduct(u: product, img: any){
    let uid = this.authService.getUser()
    u._id = this.aFire.createId();
    if(img){
      this.af.upload(`/shop/${uid}/${u._id}`, img).then(result =>{
        result.ref.getDownloadURL().then(url =>{
          u.imgUrl = url
          return this.aFire.collection(`shop/${uid}/product`).doc(u._id).set(u);
        })
      })
    }else{
      this.aFire.collection(`shop/${uid}/product`).doc(u._id).set(u);
    }    
    
  } 

  listProduct(): Observable<product[]>{
    let uid = this.authService.getUser()
    let productCollection: AngularFirestoreCollection<product> = this.aFire.collection(`shop/${uid}/product`)
    return productCollection.valueChanges()
  }

}
