import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { card } from '../minha-loja/card.model';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  private cardCollection: AngularFirestoreCollection<card> = this.aFire.collection('card')


  constructor(
    private aFire: AngularFirestore, 
    private snackBar: MatSnackBar
  ) { }

  showMessage(msg: string): void {
    this.snackBar.open(msg, 'OK', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }

  getCard():  Observable<card[]>{
    return this.cardCollection.valueChanges()
  }






}
