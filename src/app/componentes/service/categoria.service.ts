import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { categoria } from '../categoria/categoria.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private categoriaCollection: AngularFirestoreCollection<categoria> = this.aFire.collection('categoria')

  constructor(
    private aFire: AngularFirestore, 
    private snackBar: MatSnackBar
    ) { }

  showMessage(msg: string): void {
    this.snackBar.open(msg, 'OK', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }

  getCategoria():  Observable<categoria[]>{
    return this.categoriaCollection.valueChanges()
  }

  addCategoria(c: categoria) {
    c._id = this.aFire.createId();
    return this.categoriaCollection.doc(c._id).set(c);
  }

  editCategoria(c: categoria) {
    return this.categoriaCollection.doc(c._id).set(c);
  }

  filter(): Observable<categoria[]> {
    return this.aFire.collection<categoria>('categoria', ref => ref.orderBy('_id').startAt('1').endAt('1' + '\uf8ff')).valueChanges();
  }

  del(p: categoria){
    return this.categoriaCollection.doc(p._id).delete();
  }

}
