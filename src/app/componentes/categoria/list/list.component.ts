import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CategoriaService } from '../../service/categoria.service';
import { categoria } from '../categoria.model';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, AfterViewInit{

  categoria$: Observable<categoria[]>;

  total: number = 0
  gerando: boolean = false

  displayedColumns: string[] = ['_id','id', 'titulo', 'ano', 'valor', 'acoes'];
  dataSource: MatTableDataSource<categoria>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private categoriaService: CategoriaService,
    private router: Router
    ) { 
      
      this.gerando = true

      this.categoria$ = this.categoriaService.getCategoria()
      this.categoria$.subscribe(element => {
        this.dataSource.data = element
      });
      this.dataSource = new MatTableDataSource();
      
      this.gerando = false
     }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  

  navegar(id: any){
    
  }

  edit(p: categoria){
    console.log(p)
  }
  delete(p: categoria){
    this.categoriaService.del(p)
  }

}
