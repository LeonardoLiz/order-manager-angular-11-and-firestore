export interface User {
    _id?: string,
    id?: string,
    firstname: string,
    lastname: string,
    address: string,
    city: string,
    state: string,
    phone: string
    email: string
    password?: string
}