import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './auth/auth-guard.service';
import { CriarLojaComponent } from './componentes/criar-loja/criar-loja.component';
import { DetalhePedidoComponent } from './componentes/detalhe-pedido/detalhe-pedido.component';
import { ListarProdutosComponent } from './componentes/listar-produtos/listar-produtos.component';
import { MinhaLojaComponent } from './componentes/minha-loja/minha-loja.component';
import { PedidosComponent } from './componentes/pedidos/pedidos.component';
import { ProdutosComponent } from './componentes/produtos/produtos.component';
import { ViewComponent } from './componentes/view/view.component';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {path: 'criar/loja', component: CriarLojaComponent, canActivate: [AuthGuardService]},
  {path: 'minha/loja', component: MinhaLojaComponent, canActivate: [AuthGuardService]},
  {path: 'minha/loja/pedidos', component: PedidosComponent, canActivate: [AuthGuardService]},
  {path: 'minha/loja/pedido/:id', component: DetalhePedidoComponent, canActivate: [AuthGuardService]},
  {path: 'minha/loja/produtos', component: ListarProdutosComponent, canActivate: [AuthGuardService]},
  {path: 'minha/loja/produtos/criar', component: ProdutosComponent, canActivate: [AuthGuardService]},


  {path: 'main', loadChildren: './main/main.module#MainModule',  canActivate: [AuthGuardService]},
  {path: '**', component: NotFoundComponent , canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
