import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading: boolean = false

  loginForm: FormGroup = this.fb.group({
    'email': ['', [Validators.required, Validators.email ]],
    'password': ['', [Validators.required, Validators.minLength(6) ]]
  })

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.loading = true
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password).subscribe(u => {
      this.authService.showMessage('Login Efetuado com sucesso')
      this.router.navigate([`/minha/loja`])
      this.loading = false
    }, (err)=> {
      this.authService.showMessage('ERROR: ' + err)
      this.loading = false
    })
  }

  loginGoogle() {

  }



}
