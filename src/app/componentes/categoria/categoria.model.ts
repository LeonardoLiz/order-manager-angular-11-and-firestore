export interface categoria {
    _id: string,
    id: number,
    titulo: string
    ano: string,
    valor: string
}