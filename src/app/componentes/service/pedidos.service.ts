import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { order } from '../pedidos/pedido.model';
import { AuthService } from 'src/app/auth/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {


  constructor(
    private authService: AuthService,
    private aFire: AngularFirestore, 
    private snackBar: MatSnackBar
    ) { }

  showMessage(msg: string): void {
    this.snackBar.open(msg, 'OK', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }


  getOrder():  Observable<order[]>{
      let uid = this.authService.getUser()
      return this.aFire.collection<order>(`shop/${uid}/order/`, ref => ref.orderBy('view').startAt(false).endAt(false + '\uf8ff')).valueChanges();
  }

  findOrder(id: string | undefined| null): Observable<order[]>{
    let uid = this.authService.getUser()
    return this.aFire.collection<order>(`shop/${uid}/order/`, ref => ref.orderBy('_id').startAt(id).endAt(id + '\uf8ff')).valueChanges();
  }

  view(o: order){
    o.view = true
    let uid = this.authService.getUser()
    let orderShopCollection: AngularFirestoreCollection<order> = this.aFire.collection(`shop/${uid}/order`)
    orderShopCollection.doc(o._id).set(o);
  }

  getNotification(): Observable<order[]>{
    let uid = this.authService.getUser()
    return this.aFire.collection<order>(`shop/${uid}/order/`, ref => ref.orderBy('view').startAt(false).endAt(false)).valueChanges();
  }


}
