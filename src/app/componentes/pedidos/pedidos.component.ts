import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PedidosService } from '../service/pedidos.service';
import { order } from './pedido.model';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit, OnDestroy {

  step =-1;
  pedidos: number = 0
  ped: Boolean = false

  order$!: Observable<order[]>
  orders!: order[] 

  onDestroy$ = new Subject();

  constructor(
    private pedidosService: PedidosService,
    private router: Router
  ) { 
    this.order$ = this.pedidosService.getOrder()
    this.order$.pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.orders = result
      if(result.length > this.pedidos && this.ped){
        this.playAudio()
      }
      this.ped = true
    })
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.onDestroy$.next()
    this.onDestroy$.complete()
  }

  playAudio(){
    let audio = new Audio();
    audio.src = "../../../assets/audio/notification.mp3";
    audio.load();
    audio.play();
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  navegar(o: order){
    this.ped= false
    this.router.navigate([`/minha/loja/pedido/${o._id}`])
    if(!o.view)
      this.pedidosService.view(o)
  }

}
