import { M } from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { ProdutosService } from '../service/produtos.service';
import { product } from './produto.model';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit {

  imageURL!: string;
  ProductForm: FormGroup;
  arqui: any

  constructor(
    public fb: FormBuilder,
    private produtosService: ProdutosService,
    private router: Router
    ) {
    this.ProductForm = this.fb.group({
      '_id': [null],
      'name': ['', [Validators.required]],
      'value': ['0.00', [Validators.required]],
      'type': ['', [Validators.required]]
    })
  }

  ngOnInit(): void { 

  }


  // Image Preview
  showPreview(event: any) {
    const reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.arqui = file
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageURL = reader.result as string;
      }
    }
  }


  // Submit Form
  onSubmit() {
    let p: product = this.ProductForm.value;
    this.produtosService.createProduct(p, this.arqui)
    this.router.navigate([`/minha/loja/produtos`])
  }


}
