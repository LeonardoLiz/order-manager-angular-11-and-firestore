import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from './auth/service/auth.service';
import { User } from './auth/User.model';
import { shop } from './componentes/criar-loja/loja.model';
import { LojaService } from './componentes/service/loja.service';
import { PedidosService } from './componentes/service/pedidos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy  {
  title = 'fire2';
  user$!: Observable<User>;
  authenticated$!: Observable<boolean>
  shop$!: Observable<shop[]>
  onDestroy$ = new Subject();
  shop: shop = {
    address: ``,
    cpf: ``,
    description: ``,
    email: ``,
    phone: ``,
    shop: ``, 
    specialization: ``, 
  }
  num: Number = 0

  constructor(
    private authService: AuthService,
    private router: Router,
    private pedidoService: PedidosService,
    private lojaService: LojaService
  ) {
    this.authenticated$ = this.authService.getAuth()
    this.authService.getUser()
    this.authenticated$.pipe(takeUntil(this.onDestroy$)).subscribe(() =>{
      if(this.authenticated$){
        this.getShop()
      }
        
    }) 
  }


  ngOnDestroy(): void {
    this.onDestroy$.next()
    this.onDestroy$.complete()
  }

  logout() {
    this.authService.logout()
    this.router.navigate([`/auth/login`])
  }

  getShop(){
    this.shop$ = this.lojaService.getShop()
    this.shop$.pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.shop = result[0]
    })
  }
  nav(){
    this.router.navigate([`/minha/loja`])
  }

  

}
