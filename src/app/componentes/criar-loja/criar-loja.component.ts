import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LojaService } from '../service/loja.service';
import { shop } from './loja.model'


import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'app-criar-loja',
  templateUrl: './criar-loja.component.html',
  styleUrls: ['./criar-loja.component.css']
})
export class CriarLojaComponent implements OnInit {
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: string[] = ['Vendas'];
  allFruits: string[] = ['Moveis', 'Veiculos', 'Serviço de automação', 'Manutenção em Redes', 'Desenvolvimento Web'];

  @ViewChild('fruitInput')
  fruitInput!: ElementRef<HTMLInputElement>;

  loading: boolean = false
  x: boolean = false

  formRegister: FormGroup = this.fb.group({
    'address': ['', [Validators.required]],
    'cpf': ['', [Validators.required]],
    'description': ['', [Validators.required]],
    'email': ['', [Validators.required, Validators.email]],
    'phone': ['', [Validators.required]],
    'mobilephone': ['', [Validators.required,]],
    'shop': ['', [Validators.required, ]],
    specialization: this.fb.array(['Vendas'], [Validators.required]),
  })

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private lojaService: LojaService
    
  ) {  
    this.lojaService.lojaRegist()    
    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
    startWith(null),
    map((fruit: string | null) => (fruit ? this._filter(fruit) : this.allFruits.slice())),
  );}

  ngOnInit(): void {

    
  
    
  }

  onSubmit(){
    let loja: shop = this.formRegister.value
    this.lojaService.createShop(loja)
    this.router.navigate([`/minha/loja`])
  }


  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.fruits.push(value);
      this.formRegister.value.specialization.push(value);
    }

    // Clear the input value

    this.fruitCtrl.setValue(null);

  }

  remove(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
      this.formRegister.value.specialization.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.viewValue);
    this.formRegister.value.specialization.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFruits.filter(fruit => fruit.toLowerCase().includes(filterValue));
  }
}
