export interface order {
    _id: string
    receiver: string
    address: string
    status: string
    payment: string
    product: string
    view: boolean
}