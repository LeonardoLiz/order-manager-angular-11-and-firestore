export interface product {
    _id?: string,
    name: string,
    value: string,
    imgUrl: string,
    type: string
}