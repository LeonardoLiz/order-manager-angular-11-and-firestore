export interface shop {
    _id?: string
    address: string
    cpf: string
    description: string
    email: string
    phone: string
    shop: string 
    specialization: string 
}