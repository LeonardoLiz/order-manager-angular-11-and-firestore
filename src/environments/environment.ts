// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'categorias-7e55b',
    appId: '1:174889873897:web:042edcce2bfad8f5261b78',
    storageBucket: 'categorias-7e55b.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyD9rteHuvyDe19FNvKz3iNQ_pKTTJOCQ08',
    authDomain: 'categorias-7e55b.firebaseapp.com',
    messagingSenderId: '174889873897',
    measurementId: 'G-E70Y5N44LL',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
