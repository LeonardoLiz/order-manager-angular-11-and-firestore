import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/service/auth.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor(
    private authservice: AuthService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.authservice.showMessage('Rota nao encontrada, Redrecionando...')
    this.router.navigate([`/minha/loja`])
  }

}
