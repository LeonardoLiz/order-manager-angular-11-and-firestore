import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { order } from '../pedidos/pedido.model';
import { CardService } from '../service/card.service';
import { LojaService } from '../service/loja.service';
import { PedidosService } from '../service/pedidos.service';
import { card } from './card.model';

@Component({
  selector: 'app-minha-loja',
  templateUrl: './minha-loja.component.html',
  styleUrls: ['./minha-loja.component.css']
})
export class MinhaLojaComponent implements OnInit, OnDestroy {
  onDestroy$ = new Subject();
  card$: Observable<card[]>
  cards: card[] | undefined
  notification$!: Observable<order[]>
  num: number = 0
  

  constructor(
    private cardService: CardService,
    private pedidoService: PedidosService,
    private lojaService: LojaService
  ) { 
    this.card$ = this.cardService.getCard()
    this.card$.pipe(takeUntil(this.onDestroy$)).subscribe(resutl => {
      this.cards = resutl
    })
    this.notification$ = this.pedidoService.getNotification()
    this.notification$.pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.num = result.length
    })
    
  }

  ngOnInit(): void {
    this.lojaService.regist()
  }

  ngOnDestroy(): void {
      this.onDestroy$.next()
      this.onDestroy$.complete()
  }



}
