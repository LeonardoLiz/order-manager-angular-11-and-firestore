import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { product } from '../produtos/produto.model';
import { ProdutosService } from '../service/produtos.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-listar-produtos',
  templateUrl: './listar-produtos.component.html',
  styleUrls: ['./listar-produtos.component.css']
})
export class ListarProdutosComponent implements OnInit, OnDestroy {

  products$!: Observable<product[]>
  onDestroy$ = new Subject()

  gerando: boolean = false


  displayedColumns: string[] = ['imgUrl', 'name', 'value', 'type'];
  dataSource: MatTableDataSource<product>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private produtoService: ProdutosService,
    private router: Router
  ) { 
    this.products$ = this.produtoService.listProduct()
    this.products$.pipe(takeUntil(this.onDestroy$)).subscribe(result => {
      this.dataSource.data = result
    });
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.onDestroy$.next()
    this.onDestroy$.complete()
  }

  addProduct(){
    this.router.navigate([`/minha/loja/produtos/criar`])
  }
  
  navegar(u: any){

  }

}
