import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { order } from '../pedidos/pedido.model';
import { PedidosService } from '../service/pedidos.service';

@Component({
  selector: 'app-detalhe-pedido',
  templateUrl: './detalhe-pedido.component.html',
  styleUrls: ['./detalhe-pedido.component.css']
})
export class DetalhePedidoComponent implements OnInit {

  id: string | undefined | null
  

  order: order = {
    product: '',
    address: '',
    receiver: '',
    status: '',
    payment: '',
    _id: '',
    view: false
  }

  constructor(
    private pedidosService: PedidosService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
  ) { 
    this.id = this.route.snapshot.paramMap.get('id') 
    this.pedidosService.findOrder(this.id).subscribe(result => {
      this.order = result[0]
    })
  }

  ngOnInit(): void {
  }

}
