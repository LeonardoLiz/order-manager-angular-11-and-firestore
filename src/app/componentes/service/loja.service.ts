import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/service/auth.service';
import { shop } from '../criar-loja/loja.model';

@Injectable({
  providedIn: 'root'
})
export class LojaService {

  constructor(
    private authService: AuthService,
    private aFire: AngularFirestore, 
    private snackBar: MatSnackBar,
    private router: Router
  ) { }


  showMessage(msg: string): void {
    this.snackBar.open(msg, 'OK', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }


  createShop(shop: shop) {
    let uid = this.authService.getUser()
    shop._id = this.aFire.createId()
    this.aFire.collection(`shop/${uid}/shop`).doc(uid).set(shop)
  }


  getShop(): Observable<any[]>{
    let uid = this.authService.getUser()
    return this.aFire.collection(`shop/${uid}/shop`).valueChanges()
  }

  lojaRegist(){
    let uid = this.authService.getUser()
      let shop: AngularFirestoreCollection<shop> = this.aFire.collection(`shop/${uid}/shop`)
      shop.valueChanges().subscribe(async result => {
         if (result.length > 0){
          this.router.navigate([`/minha/loja`])
        }
      })
    

  }
  regist(){
    let uid = this.authService.getUser()
      let shop: AngularFirestoreCollection<shop> = this.aFire.collection(`shop/${uid}/shop`)
      shop.valueChanges().subscribe(async result => {
         if (result.length < 1){
          this.router.navigate([`/criar/loja`])
        }
      })
    

  }
}
