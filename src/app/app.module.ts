import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';

import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NgxCurrencyModule } from "ngx-currency";

import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth'
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore'

import { ListComponent } from './componentes/categoria/list/list.component';
import { AddComponent } from './componentes/categoria/add/add.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ViewComponent } from './componentes/view/view.component';
import { AuthModule } from './auth/auth.module';
import { MinhaLojaComponent } from './componentes/minha-loja/minha-loja.component';
import { PedidosComponent } from './componentes/pedidos/pedidos.component';
import { DetalhePedidoComponent } from './componentes/detalhe-pedido/detalhe-pedido.component';
import { ProdutosComponent } from './componentes/produtos/produtos.component';
import { CriarLojaComponent } from './componentes/criar-loja/criar-loja.component';
import { ListarProdutosComponent } from './componentes/listar-produtos/listar-produtos.component';

const maskConfig: Partial<IConfig> = {
  validation: true,
};


@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    AddComponent,
    NotFoundComponent,
    ViewComponent,
    MinhaLojaComponent,
    PedidosComponent,
    DetalhePedidoComponent,
    ProdutosComponent,
    CriarLojaComponent,
    ListarProdutosComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AuthModule,

    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatProgressBarModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    MatSelectModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatBadgeModule,

    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,

    NgxMaskModule.forRoot({dropSpecialCharacters: false}),
    NgxCurrencyModule,


    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
