import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoriaService } from '../../service/categoria.service';
import { categoria } from '../categoria.model';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  categoriaForm = this.fb.group({
    id: [undefined],
    titulo: ['', [Validators.required]],
    ano: ['', [Validators.required] ],
    valor: ['0.00', [Validators.required]]
  })

  constructor(
    private fb: FormBuilder,
    private categoriaService:  CategoriaService,
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    let p: categoria = this.categoriaForm.value;
    if(!p._id)
      this.addCat(p)
    else
      this.edit(p)
  }

  addCat(p: categoria){
    this.categoriaService.addCategoria(p)
      .then(() => {
        this.categoriaService.showMessage("Categoria Adicionada")
        this.categoriaForm.reset({id: '', titulo: '', ano: '', valor: ''})
      })
      .catch(() => {
        this.categoriaService.showMessage("ERROR: Erro ao adcionar categoria!")
        this.categoriaForm.reset({id: '', titulo: '', ano: '', valor: ''})
      })
  }
  edit(p: categoria){
    this.categoriaService.addCategoria(p)
      .then(() => {
        this.categoriaService.showMessage("Categoria Editada")
        this.categoriaForm.reset({id: '', titulo: '', ano: '', valor: ''})
      })
      .catch(() => {
        this.categoriaService.showMessage("ERROR: Erro ao editar categoria!")
        this.categoriaForm.reset({id: '', titulo: '', ano: '', valor: ''})
      })
  }



}
