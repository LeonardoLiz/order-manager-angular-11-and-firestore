export interface card {
    _id?: string
    title: string,
    link: string,
    subtitle: string
}